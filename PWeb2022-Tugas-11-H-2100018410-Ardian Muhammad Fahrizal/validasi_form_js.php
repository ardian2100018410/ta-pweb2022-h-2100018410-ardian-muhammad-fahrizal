<!DOCTYPE html>
<html>
<head>
	<title>Validasi Form JavaScript</title>
	<link rel="stylesheet" type="text/css" href="validasi form js css.css">
</head>
<body>
	<center>
		<h2>Portal UAD</h2>
	</center>
	<div class="login">
		<form action="#" method="POST" onsubmit="validasi()">
			<div>
				<label>Nama :</label>
				<input type="text" name="nama" id="nama" />
			</div>
			<div>
				<label>Umur :</label>
				<input type="text" name="umur" id="umur" />
			</div>
			<div>
				<label>Masukkan IPK :</label>
				<input type="text" name="ipk" id="ipk" />
			</div>

			<div>
				<label>Email :</label>
				<input type="email" name="email" id="email" />
			</div>
			<div>
				<label>Alamat :</label>
				<textarea cols="40" rows="5" name="alamat" id="alamat"></textarea>
			</div>
			<div>
				<input type="submit" value="Daftar" class="tombol">
			</div>	
		</form>
	</div>
</body>
<script type="text/javascript">
	function validasi() {
		var nama = document.getElementById('nama').value;
		var email = document.getElementById('email').value;
		var umur = document.getElementById('umur').value;
		var ipk = document.getElementById('ipk').value;
		var alamat = document.getElementById('alamat').value;
		if (nama !="" && umur!="" && ipk!="" && email!="" && alamat!="") {
			return true;
		}
		else {
			alert ('Anda harus mengisi data dengan lengkap !');
		}
	}
</script>
</html>