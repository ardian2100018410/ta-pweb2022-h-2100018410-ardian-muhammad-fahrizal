<?php
$arrmakanan=array("Sate","Mie Ayam","Bakso","Soto","Cilok");

echo "Menampilkan isi array dengan FOR: <br>";
for ($i=0; $i < count($arrmakanan) ; $i++) { 
	echo "Makanan <font color=$arrmakanan[$i]>" .$arrmakanan[$i]. "</font><br>";
}

echo "<br>Menampilkan isi array dengan FOREACH :<br>";
foreach ($arrmakanan as $makanan) {
	echo "Makanan <font color=$makanan>". $makanan. "</font><br>";
}
?>