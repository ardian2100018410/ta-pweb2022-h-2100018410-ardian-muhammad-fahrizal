<?php
$arrnilai = array("Ardian"=>100,"Sanri"=>490,"Eka"=>75,"Hamid"=>567);
echo "<b>Array sebelum diurutkan</br>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";

asort($arrnilai);
reset($arrnilai);
echo "<b>Array setelah diurutkan dengan asort()</b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";

arsort($arrnilai);
reset($arrnilai);
echo "<b>Array setelah diurutkan dengan arsort()</b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";
?>