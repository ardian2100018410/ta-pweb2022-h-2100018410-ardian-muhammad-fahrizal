<?php
$arrnilai = array("Ardian"=>100,"Sanri"=>490,"Eka"=>75,"Hamid"=>567);
echo "<b>Array sebelum diurutkan</br>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";

ksort($arrnilai);
reset($arrnilai);
echo "<b>Array setelah diurutkan dengan ksort()</b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";

krsort($arrnilai);
reset($arrnilai);
echo "<b>Array setelah diurutkan dengan krsort()</b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";
?>