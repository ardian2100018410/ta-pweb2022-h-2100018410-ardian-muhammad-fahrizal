<?php
$arrnilai = array("Ardian"=>100,"Sanri"=>490,"Eka"=>75,"Hamid"=>567);
echo "<b>Array sebelum diurutkan</br>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";

sort($arrnilai);
reset($arrnilai);
echo "<b>Array setelah diurutkan dengan sort()</b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";

rsort($arrnilai);
reset($arrnilai);
echo "<b>Array setelah diurutkan dengan rsort()</b>";
echo "<pre>";
print_r($arrnilai);
echo "</pre>";
?>